# Build Environment
FROM node:8 as builder
ARG YARN_ENV=development

WORKDIR /home/app
COPY package.json yarn.lock ./
RUN yarn
COPY src/ ./src
COPY nginx.conf ./
COPY public/ ./public
RUN yarn build:${YARN_ENV}

# Production
FROM nginx:1.15.5
COPY --from=builder /home/app/build/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
CMD ["nginx", "-g", "daemon off;"]
EXPOSE 80
